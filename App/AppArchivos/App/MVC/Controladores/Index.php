<?php

/**
 * Clase: Index
 */
class Index extends Controlador
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
    }

    /**
     * Metodo: Index
     */
    public function Index()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP)));
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Login', 'Index.html')));
        unset($Plantilla);
    }

    public function Autenticacion() {
        if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == AppFechas::ObtenerFechaActual()) == true) {
            unset($_POST['Key']);
            if(AppPost::DatosVacios($_POST) == false) {
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $Consulta = $this->Modelo->ConsultarUsuario($DatosPost['username'], hash('sha256', $DatosPost['password']));
                if($Consulta['Cantidad'] == 1) {
                    $ConsultaPermisos = $this->Modelo->ConsultarPermisos($Consulta[0]['IdPerfil']);
                    if($ConsultaPermisos['Cantidad'] == 1) {
                        AppSession::Registrar($Consulta[0], $ConsultaPermisos[0]);
                        unset($DatosPost, $Consulta, $ConsultaPermisos);
                        header("Location: ".NeuralRutasApp::RutaUrlAppModulo('Central'));
                        exit();
                    }
                    else {
                        unset($DatosPost, $Consulta, $ConsultaPermisos);
                        header("Location: ".NeuralRutasApp::RutaUrlApp('LogOut'));
                        exit();
                    }
                }
            }
        }
    }
}