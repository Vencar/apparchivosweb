<?php

/**
 * Clase: Index_Modelo
 */
class Index_Modelo extends Modelo
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
    }

    /**
     * Metodo: Ejemplo
     */
    public function ConsultarUsuario($username = false, $password = false)
    {
        if ($username and $password) {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_sistema_usuarios');
            $Consulta->Columnas('tbl_informacion_usuarios.IdUsuario, IdInformacion, Usuario, IdPerfil, Nombres, ApellidoPaterno,
                             ApellidoMaterno');
            $Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario',
                'tbl_informacion_usuarios.IdUsuario');
            $Consulta->Condicion("tbl_sistema_usuarios.Usuario = '$username'");
            $Consulta->Condicion("tbl_sistema_usuarios.Password = '$password'");
            $Consulta->Condicion("tbl_sistema_usuarios.Status = 'ACTIVO'");
            return $Consulta->Ejecutar(true, true);
        }
    }

    public function ConsultarPermisos($IdPerfil = false)
    {
        if ($IdPerfil and is_numeric($IdPerfil)) {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Columnas('Nombre, Central, Error, Administrador, Usuario, Status');
            $Consulta->Tabla('tbl_sistema_usuarios_perfil');
            $Consulta->Condicion("IdPerfil = '$IdPerfil'");
            return $Consulta->Ejecutar(true, true);
        }
    }
}