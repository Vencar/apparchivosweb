<?php

class AppDropzone
{

    public static function SubirArchivo($Archivo = false)
    {
        if ($Archivo == true AND is_array($Archivo)) {
            if (AppDropzone::ValidarExtencion($Archivo['file']['name'])) {
                $Prefijo = substr(md5(uniqid(rand())), 0, 6);
                $Nombres = [];
                $Nombres['RutaArchivo'] = $Prefijo . "_" . AppDropzone::FormatoNombreArchivo($Archivo['file']['name']);
                $Nombres['Original'] = $Archivo['file']['name'];
                $Destino = implode(DIRECTORY_SEPARATOR, array(__SysNeuralFileRootApp__, APP, 'Web', 'Documentos', $Nombres['RutaArchivo']));
                return ((move_uploaded_file($Archivo['file']['tmp_name'], $Destino)) ? $Nombres : false);
            } else {
                return false;
            }
        }
    }

    public static function ValidarExtencion($NombreArchivo = false)
    {
        if ($NombreArchivo == true) {
            $Extencion = explode(".", $NombreArchivo);
            $Extencion = $Extencion[count($Extencion) - 1];
            return (($Extencion == "pdf" || $Extencion == "xlsx") ? true : false);
        }

    }

    public static function ObtenerExtensionArchivo($Nombre = false)
    {
        if ($Nombre == true) {
            $Extencion = explode(".", $Nombre);
            return $Extencion = $Extencion[count($Extencion) - 1];
        }
    }

    public static function LimpiarPrefijo($Nombre = false)
    {
        if ($Nombre == true) {
            return $NombreArchivo = explode("_", $Nombre)[1];
        }
    }

    public static function RutaArchivo($NombreArchivo = false)
    {
        if ($NombreArchivo == true) {
            return NeuralRutasApp::WebPublico() . "Documentos/" . $NombreArchivo;
        }
    }

    public static function FormatoNombreArchivo($Nombre = false)
    {
        if ($Nombre == true) {
            $Cadena = str_ireplace('_', '', $Nombre);
            $Cadena = str_ireplace('-', '', $Cadena);
            $Cadena = str_ireplace('/', '', $Cadena);
            $Cadena = str_ireplace('*', '', $Cadena);
            $Cadena = str_ireplace('?', '', $Cadena);
            $Cadena = str_ireplace('¿', '', $Cadena);
            $Cadena = str_ireplace('¿', '', $Cadena);
            $Cadena = str_ireplace('%', '', $Cadena);
            $Cadena = str_ireplace('--', '', $Cadena);
            $Cadena = str_ireplace('^', '', $Cadena);
            $Cadena = str_ireplace('[', '', $Cadena);
            $Cadena = str_ireplace(']', '', $Cadena);
            $Cadena = str_ireplace("\\", '', $Cadena);
            $Cadena = str_ireplace('=', '', $Cadena);
            $Cadena = str_ireplace('+', '', $Cadena);
            $Cadena = str_ireplace(' ', '', $Cadena);
            return $Cadena = str_ireplace('&', '', $Cadena);
        }
    }

}