<?php

    /**
     * VoIP Technology S.A.
     * Control De ERP, Para la empresa Agua Soda LTDA.
     *
     * @copyright Copyright (c) 2018, VoIP Technology S.A.
     * @link   http://voiptech.com.mx
     * @author Ramses Aguirre Farrera <ramsesaguirre2012@gmail.com>
     * @author Contacto <contacto#voiptech.com.mx>
     * @license Contrato de Licencia de Software de Usuario Final (“EULA”).
     * @license Incluida licencia carpeta de Informacion
     * @version 1.0
     *
     * Este contrato de licencia de software de usuario final (EULA, por sus siglas en inglés)
     * es un acuerdo vinculante entre el usuario titular de la licencia (“Usuario final”) y VoIP Technology S.A.,
     * que expone los términos y condiciones que rigen el uso y el funcionamiento de los productos
     * de software de computadoras propiedad de CallOne Contact Center (el “Software”) y las especificaciones técnicas
     * escritas para el uso y el funcionamiento del Software (la “Documentación”). Donde el sentido
     * y el contexto lo permitan, las referencias en este EULA al Software incluyen la Documentación.
     * Al descargar e instalar, copiar o, en otras palabras, usar el Software y/o aceptar este EULA,
     * el Usuario final acuerda reconocer como vinculante los términos y condiciones de este EULA.
     *
     * Si el Usuario final no acuerda ni acepta los términos de este EULA, es posible que el Usuario
     * final no tenga acceso ni pueda usar el Software.
     *
     */

	class AppLogSistema extends AppSQLConsultas {

        /**
         * Metodo Publico
         * RegistrarInicioSesion($InformacionSesion = false)
         * 
         * Crea un registro de inicio de sesion del usuario.
         * @param bool $InformacionSesion
         */
	    public static function RegistrarInicioSesion($InformacionSesion = false){
	        if($InformacionSesion == true AND is_array($InformacionSesion)){
                $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
                $Conexion->insert('tbl_log_sesiones', array(
                        'IdInformacionUsuario'=>$InformacionSesion['Informacion']['IdInformacion'],
                        'FechaHoraInicio'=>AppFechas::ObtenerDatetimeActual(),
                        'Observaciones'=>"Inicio de sesión exitoso.",
                ));
                unset($Conexion);
            }
        }

        /**
         * Metodo Publico
         * RegistrarFinSesionHost($IdInformacionUsuario)
         *
         * Finaliza la sesion del usuario
         * @param $IdInformacionUsuario
         */
        public static function RegistrarFinSesionHost($IdInformacionUsuario = false){
            if(isset($IdInformacionUsuario) == true and $IdInformacionUsuario != ''){
                $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
                $UltimaSesion = self::ConsultarUltimaSesion($Conexion, array('IdInformacionUsuario'=>$IdInformacionUsuario));
                $Datos = array(
                    'FechaHoraFin' => AppFechas::ObtenerDatetimeActual(),
                    'Status' => 'TERMINADO',
                    'Observaciones'=>"Sesión terminada por el servidor.",
                );
                $Condiciones = array(
                    'IdInformacionUsuario' => $IdInformacionUsuario,
                    'IdSesion' => $UltimaSesion[0]['IdSesion'],
                );
                $Conexion->update('tbl_log_sesiones', $Datos, $Condiciones);
                unset($Conexion, $UltimaSesion, $Datos, $Condiciones, $IdInformacionUsuario);
            }
        }

        /**
         * Metodo Publico
         * RegistrarFinSesionInactivo($IdInformacionUsuario)
         *
         * Finaliza la sesion del usuario por inactividad
         * @param $IdInformacionUsuario
         */
        public static function RegistrarFinSesionInactivo($IdInformacionUsuario = false){
            if(isset($IdInformacionUsuario) == true and $IdInformacionUsuario != ''){
                $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
                $UltimaSesion = self::ConsultarUltimaSesion($Conexion, array('IdInformacionUsuario'=>$IdInformacionUsuario));
                $Datos = array(
                    'FechaHoraFin' => AppFechas::ObtenerDatetimeActual(),
                    'Status' => 'TERMINADO',
                    'Observaciones'=>"Sesión terminada por inactividad.",
                );
                $Condiciones = array(
                    'IdInformacionUsuario' => $IdInformacionUsuario,
                    'IdSesion' => $UltimaSesion[0]['IdSesion'],
                );
                $Conexion->update('tbl_log_sesiones', $Datos, $Condiciones);
                unset($Conexion, $UltimaSesion, $Datos, $Condiciones, $IdInformacionUsuario);
            }
        }

        /**
         * Metodo Publico Estatico
         * RegistrarFinSesion($InformacionSesion = false)
         *
         * Actualiza el termino de la ultima sesion del usuario.
         * @param bool $InformacionSesion
         */
        public static function RegistrarFinSesion($InformacionSesion = false){
            if ($InformacionSesion == true AND is_array($InformacionSesion)) {
                $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
                $UltimaSesion = self::ConsultarUltimaSesion($Conexion, array('IdInformacionUsuario'=>$InformacionSesion['Informacion']['IdInformacion']));
                $Datos = array(
                    'FechaHoraFin' => AppFechas::ObtenerDatetimeActual(),
                    'Status' => 'TERMINADO',
                    'Observaciones'=>"Sesión terminada por el usuario.",
                );
                $Condiciones = array(
                    'IdInformacionUsuario' => $InformacionSesion['Informacion']['IdInformacion'],
                    'IdSesion' => $UltimaSesion[0]['IdSesion'],
                );
                $Conexion->update('tbl_log_sesiones', $Datos, $Condiciones);
                unset($Conexion, $UltimaSesion, $Datos, $Condiciones);
            }
        }

        /**
         * Metodo Publico Estatico
         * ConsultarUltimaSesion($Conector = false, $Condiciones = false)
         *
         * Devuelve el registro de la ultima sesion del usuario.
         * @param bool $Conector
         * @param bool $Condiciones
         * @return mixed
         */
        private static function ConsultarUltimaSesion($Conector = false, $Condiciones = false){
            if($Conector == true AND $Condiciones == true AND is_array($Condiciones) == true){
                $Condiciones = self::ObtenerCondicionesAND($Condiciones);
                $SQL = "SELECT tbl_log_sesiones.IdSesion, tbl_log_sesiones.FechaHoraInicio FROM tbl_log_sesiones".
                    " WHERE ".$Condiciones . " AND tbl_log_sesiones.FechaHoraInicio = (SELECT MAX(tbl_log_sesiones.FechaHoraInicio) ".
                    " FROM tbl_log_sesiones WHERE ".$Condiciones.")";
                $Consulta = $Conector->prepare($SQL);
                $Consulta->execute();
                return $Consulta->fetchAll(PDO::FETCH_ASSOC);
            }
        }
	}