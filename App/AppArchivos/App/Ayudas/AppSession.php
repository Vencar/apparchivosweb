<?php

    /**
     * VoIP Technology S.A.
     * Control De ERP, Para la empresa Agua Soda LTDA.
     *
     * @copyright Copyright (c) 2018, VoIP Technology S.A.
     * @link   http://voiptech.com.mx
     * @author Ramses Aguirre Farrera <ramsesaguirre2012@gmail.com>
     * @author Contacto <contacto#voiptech.com.mx>
     * @license Contrato de Licencia de Software de Usuario Final (“EULA”).
     * @license Incluida licencia carpeta de Informacion
     * @version 1.0
     *
     * Este contrato de licencia de software de usuario final (EULA, por sus siglas en inglés)
     * es un acuerdo vinculante entre el usuario titular de la licencia (“Usuario final”) y VoIP Technology S.A.,
     * que expone los términos y condiciones que rigen el uso y el funcionamiento de los productos
     * de software de computadoras propiedad de CallOne Contact Center (el “Software”) y las especificaciones técnicas
     * escritas para el uso y el funcionamiento del Software (la “Documentación”). Donde el sentido
     * y el contexto lo permitan, las referencias en este EULA al Software incluyen la Documentación.
     * Al descargar e instalar, copiar o, en otras palabras, usar el Software y/o aceptar este EULA,
     * el Usuario final acuerda reconocer como vinculante los términos y condiciones de este EULA.
     *
     * Si el Usuario final no acuerda ni acepta los términos de este EULA, es posible que el Usuario
     * final no tenga acceso ni pueda usar el Software.
     *
     */

	class AppSession {

		private static $_Llave = 'wqpmrie8b1z9gyjirnqan2tc1qvc';
		private static $_Contenedor = false;
	//	private static $_TIEMPO_LOGIN  = 120;
//		private static $_TIEMPO_ESPERA = 60;
		private static $_TIEMPO_LOGIN = 3600;
		private static $_TIEMPO_ESPERA = 60;

		/**
		 * Metodo Publico
		 * Registrar($Usuario = false, $Permiso = false)
		 *
		 * Registra informacion de una nueva session de trabajo
		 * @param $Usuario: Arreglo de los datos del usuario
		 * @param $Permiso: Arreglo de permisos de usuario array('Central' => 'true', 'Error' => 'true', 'Empresa' => 'false')
		 *
		 * */
		public static function Registrar($Usuario = false, $Permiso = false){
			$RegistroSession = array(
				'Session' => array(
					'Llave' => implode('_', array(self::$_Llave, $Usuario['Usuario'], date("Y-m-d"))),
					'Fecha' => date("Y-m-d"),
					'Base'  => strtotime(date("Y-m-d H:i:s"))
				),
				'Informacion' => $Usuario,
				'Permiso' => $Permiso
			);
			NeuralSesiones::AsignarSession('UOAUTH_APP', $RegistroSession);
            AppLogSistema::RegistrarInicioSesion($RegistroSession);
		}

		/**
		 * Metodo Publico
		 * KeepAliveSession()
		 *
		 * Reasigna tiempo a la session
		 */
		public static function KeepAliveSession($Usuario = false, $Permiso = false){
			if($Usuario == true AND $Permiso == true){
				$Session  = self::LeerSession();
				if($Session['Informacion']['Usuario'] == $Usuario AND $Session['Permiso']['Nombre'] == $Permiso){
					$Session['Session']['Base'] = strtotime(date("Y-m-d H:i:s"));
					NeuralSesiones::AsignarSession('UOAUTH_APP', $Session);
				}
				else{
					return "Error";
				}
			}
			else
				return "Error";
		}

		/**
		 * Metodo Publico
		 * ValSessionGlobal()
		 *
		 * Valida el acceso al metodo mediante los permisos de la session
		 *
		 **/
		public static function ValSessionGlobal(){
			NeuralSesiones::Inicializar(APP);
			$ModRewrite = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$Control = (isset($ModRewrite[1]) == true) ? $ModRewrite[1] : 'Index';
			$Session = self::LeerSession();
			Ayudas::print_r($Session);
			exit();
			if(($Session['Session']['Llave'] == implode('_', array(self::$_Llave, $Session['Informacion']['Usuario'], date("Y-m-d")))) == true AND ((($Session['Session']['Base']) + self::$_TIEMPO_LOGIN) > strtotime(date("Y-m-d H:i:s"))) == true){
				if(array_key_exists($Control, $Session['Permiso']) == true){
					if($Session['Permiso'][$Control] == 'false' ){
						header("Location: ".NeuralRutasApp::RutaUrlAppModulo('Error', 'SinAcceso', 'Index'));
						exit();
					}
				}
				else {
					header("Location: ".NeuralRutasApp::RutaUrlAppModulo('Error', 'SinAcceso', 'Index'));
					exit();
				}
			}
			else {
				self::RedireccionLogOut();
				exit();
				// Enviar a un logout y redireccionar con parametro de error por dato get
			}
		}

		/**
		 * Metodo Publico
		 * Tiempo()
		 *
		 * Tiempos de la session Tiempo Restante para finalizar, y tiempo Inical asiganado de la session
		 * @return array
		 */
		public static function Tiempo(){
			$Session = self::LeerSession();
			$Tiempo = ((($Session['Session']['Base']) + self::$_TIEMPO_LOGIN) - strtotime(date("Y-m-d H:i:s"))) - self::$_TIEMPO_ESPERA;
			$Tiempos = array(
				'TiempoRestante' => $Tiempo,
				'TiempoSession' => self::$_TIEMPO_LOGIN);
			return $Tiempos;
		}

		/**
		 * Metodo Publico
		 * InfomacionSession()
		 *
		 * Entrega los datos de sesion para ver visualizados
		 *
		 * */
		public static function InfomacionSession(){
			return self::$_Contenedor;
		}

        /**
         * Metodo Publico
         * ObtenerDatosSesion()
         *
         * Devuelve la informacion sobre el usuario de la sesion.
         * @return mixed
         */
		public static function ObtenerDatosSesion(){
            return self::LeerSession();
        }

		/**
		 * Metodo Privado
		 * LeerSession()
		 *
		 * Lee la informacion de la session
		 *
		 * */
		private static function LeerSession(){
			if(is_array(self::$_Contenedor) == true) {
				return self::$_Contenedor;
			}
			else {
				return self::$_Contenedor = NeuralSesiones::ObtenerSession('UOAUTH_APP');
			}
		}

		/**
		 * Metodo Privado
		 * RedireccionLogOut()
		 *
		 * LogOut del sistema
		 *
		 * */
		private static function RedireccionLogOut(){
			header("Location: ".NeuralRutasApp::RutaUrlApp('LogOut'));
			exit();
		}

	}