<?php

/**
 * VoIP Technology S.A.
 * Control De ERP, Para la empresa Agua Soda LTDA.
 *
 * @copyright Copyright (c) 2018, VoIP Technology S.A.
 * @link   http://voiptech.com.mx
 * @author Ramses Aguirre Farrera <ramsesaguirre2012@gmail.com>
 * @author Contacto <contacto#voiptech.com.mx>
 * @license Contrato de Licencia de Software de Usuario Final (“EULA”).
 * @license Incluida licencia carpeta de Informacion
 * @version 1.0
 *
 * Este contrato de licencia de software de usuario final (EULA, por sus siglas en inglés)
 * es un acuerdo vinculante entre el usuario titular de la licencia (“Usuario final”) y VoIP Technology S.A.,
 * que expone los términos y condiciones que rigen el uso y el funcionamiento de los productos
 * de software de computadoras propiedad de CallOne Contact Center (el “Software”) y las especificaciones técnicas
 * escritas para el uso y el funcionamiento del Software (la “Documentación”). Donde el sentido
 * y el contexto lo permitan, las referencias en este EULA al Software incluyen la Documentación.
 * Al descargar e instalar, copiar o, en otras palabras, usar el Software y/o aceptar este EULA,
 * el Usuario final acuerda reconocer como vinculante los términos y condiciones de este EULA.
 *
 * Si el Usuario final no acuerda ni acepta los términos de este EULA, es posible que el Usuario
 * final no tenga acceso ni pueda usar el Software.
 *
 */

class AppConversores
{

    /**
     * Metodo Estatico Publico
     * ASCII_HEX($Ascii = false)
     *
     * Convierte una cadena de ASCII - HEX
     * @param $Ascii : Valor Ascii que se desea convertir
     * @example
     * @access public
     * */
    public static function ASCII_HEX($Ascii = false)
    {
        if ($Ascii == true) {
            $Cantidad = strlen($Ascii);
            for ($i = 0; $i < $Cantidad; $i++) {
                $Byte = dechex(ord($Ascii[$i]));
                $Hex[] = str_repeat('0', 2 - strlen($Byte)) . $Byte;
            }
            return implode('', $Hex);
        }
    }

    /**
     * Metodo Estatico Publico
     * HEX_ASCII($Hex = false)
     *
     * Convierte una Cadena HEX - ASCII
     * @param $Hex : valor Hex que se desea convertir
     * @access public
     * */
    public static function HEX_ASCII($Hex = false)
    {
        if ($Hex == true) {
            $Cantidad = strlen($Hex);
            for ($i = 0; $i < $Cantidad; $i = $i + 2) {
                $Ascii[] = chr(hexdec(substr($Hex, $i, 2)));
            }
            return implode('', $Ascii);
        }
    }


}