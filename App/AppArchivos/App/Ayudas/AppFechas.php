<?php

	class AppFechas {

		private static $TimeZone = 'America/Mexico_City';
		
		/**
		 * Metodo publico estatico
		 * ObtenerFechaActual()
		 *
		 * Obtiene la fecha actual.
		 * @return string: Fecha en formato, (Y-m-d) o (AAAA-MM-DD)
		 */
		public static function ObtenerFechaActual(){
			date_default_timezone_set(self::$TimeZone);
			$NowDateTime = new \DateTime('now');
			$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
			return $NowDateTime->format("Y-m-d");
		}

		/**
		 * Metodo publico estatico
		 * ObtenerHoraActual.
		 *
		 * Obtiene la hora actual.
		 * @return string: Hora en formato, (H:i:s) o (HH:MM:SS).
		 */
		public static function ObtenerHoraActual(){
			date_default_timezone_set(self::$TimeZone);
			$NowDateTime = new \DateTime('now');
			$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
			return $NowDateTime->format("H:i:s");
		}

		/**
		 * Metodo publico estatico
		 * ObtenerDatetimeActual()
		 *
		 * Obtiene el la fecha y hora actual en formato datetime.
		 * @return string: Hora y Fecha, Y-m-d H:i:s.
		 */
		public static function ObtenerDatetimeActual(){
			date_default_timezone_set(self::$TimeZone);
			$NowDateTime = new \DateTime('now');
			$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
			return $NowDateTime->format("Y-m-d H:i:s");
		}
		
	}


