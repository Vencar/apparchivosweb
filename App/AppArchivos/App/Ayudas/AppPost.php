<?php

    /**
     * VoIP Technology S.A.
     * Control De ERP, Para la empresa Agua Soda LTDA.
     *
     * @copyright Copyright (c) 2018, VoIP Technology S.A.
     * @link   http://voiptech.com.mx
     * @author Ramses Aguirre Farrera <ramsesaguirre2012@gmail.com>
     * @author Contacto <contacto#voiptech.com.mx>
     * @license Contrato de Licencia de Software de Usuario Final (“EULA”).
     * @license Incluida licencia carpeta de Informacion
     * @version 1.0
     *
     * Este contrato de licencia de software de usuario final (EULA, por sus siglas en inglés)
     * es un acuerdo vinculante entre el usuario titular de la licencia (“Usuario final”) y VoIP Technology S.A.,
     * que expone los términos y condiciones que rigen el uso y el funcionamiento de los productos
     * de software de computadoras propiedad de CallOne Contact Center (el “Software”) y las especificaciones técnicas
     * escritas para el uso y el funcionamiento del Software (la “Documentación”). Donde el sentido
     * y el contexto lo permitan, las referencias en este EULA al Software incluyen la Documentación.
     * Al descargar e instalar, copiar o, en otras palabras, usar el Software y/o aceptar este EULA,
     * el Usuario final acuerda reconocer como vinculante los términos y condiciones de este EULA.
     *
     * Si el Usuario final no acuerda ni acepta los términos de este EULA, es posible que el Usuario
     * final no tenga acceso ni pueda usar el Software.
     *
     */

	class AppPost {
	
		/**
		 * Metodo Publico
		 * FormatoEspacio($Array = false)
		 * 
		 * Retira los espacios tanto del inicio como del final de 
		 * los valores del array
		 * @param $Array: array tomado como $_POST o $_GET
		 * @access public
		 * 
		 * */
		public static function FormatoEspacio($Array = false) {
			if($Array == true AND is_array($Array) == true) {
				foreach ($Array AS $Llave => $Valor) {
					$Lista[trim($Llave)] = trim($Valor);
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Publico
		 * FormatoEspaciOmitido($Array = false, $Omitidos = false)
		 * 
		 * Retira los espacios tanto del inicio como del final de 
		 * los valores del array
		 * @param $Array: array tomado como $_POST o $_GET
		 * @param $Omitidos: array incremental donde se omitiran
		 * @example $Omitidos: array('Nombre', 'Nombre')
		 * @access public
		 * 
		 * */
		public static function FormatoEspaciOmitido($Array = false, $Omitidos = false) {
			if($Array == true AND is_array($Array) == true AND $Omitidos == true AND is_array($Omitidos) == true) {
				$Matriz = (is_array($Omitidos) == true) ? array_flip($Omitidos) : array();
				foreach ($Array AS $Llave => $Valor) {
					$Lista[trim($Llave)] = (array_key_exists($Llave, $Matriz) == false) ? trim($Valor) : $Valor;
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Estatico Publico
		 * FormatoMin($Array = false)
		 * 
		 * Aplica a los Valores de Array Asociativo de Primer Nivel
		 * el formato de minusculas a los valores de texto y retira los
		 * espacios del comienzo y el final del valor
		 * @param $Array: array tomado como $_POST o $_GET
		 * @access public
		 * 
		 * */
		public static function FormatoMin($Array = false) {	
			if($Array == true AND is_array($Array) == true) {
				foreach ($Array AS $Llave => $Valor) {
					$Lista[trim($Llave)] = mb_strtolower(trim($Valor));
				}
			}
			return $Lista;
		}
		
		/**
		 * Metodo Estatico Publico
		 * FormatoMinOmitido($Array = false, $Omitidos = false)
		 * 
		 * Aplica a los Valores de Array Asociativo de Primer Nivel
		 * el formato de minusculas a los valores de texto y retira los
		 * espacios del comienzo y el final del valor
		 * @param $Array: array tomado como $_POST o $_GET
		 * @param $Omitidos: array donde se omitiran
		 * @example $Omitidos: array('Nombre', 'Nombre')
		 * @access public
		 * 
		 * */
		public static function FormatoMinOmitido($Array = false, $Omitidos = false) {	
			if($Array == true AND is_array($Array) == true AND $Omitidos == true AND is_array($Omitidos) == true) {
				$Matriz = (is_array($Omitidos) == true) ? array_flip($Omitidos) : array();
				foreach ($Array AS $Llave => $Valor) {
					$Lista[trim($Llave)] = (array_key_exists($Llave, $Matriz) == true) ? trim($Valor) : mb_strtolower(trim($Valor));
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Estatico Publico
		 * FormatoMayus($Array = false)
		 * 
		 * Aplica a los Valores de Array Asociativo de Primer Nivel
		 * el formato de mayusculas a los valores de texto y retira los
		 * espacios del comienzo y el final del valor
		 * @param $Array: array tomado como $_POST o $_GET
		 * @access public
		 * 
		 * */
		public static function FormatoMayus($Array = false) {	
			if($Array == true AND is_array($Array) == true) {
				foreach ($Array AS $Llave => $Valor) {
					$Lista[trim($Llave)] = mb_strtoupper(trim($Valor));
				}
			}
			return $Lista;
		}
		
		/**
		 * Metodo Estatico Publico
		 * FormatoMayusOmitido($Array = false, $Omitidos = false)
		 * 
		 * Aplica a los Valores de Array Asociativo de Primer Nivel
		 * el formato de mayusculas a los valores de texto y retira los
		 * espacios del comienzo y el final del valor
		 * @param $Array: array tomado como $_POST o $_GET
		 * @param $Omitidos: array donde se omitiran
		 * @example $Omitidos: array('Nombre', 'Nombre')
		 * @access public
		 * 
		 * */
		public static function FormatoMayusOmitido($Array = false, $Omitidos = false) {	
			if($Array == true AND is_array($Array) == true AND $Omitidos == true AND is_array($Omitidos) == true) {
				$Matriz = (is_array($Omitidos) == true) ? array_flip($Omitidos) : array();
				foreach ($Array AS $Llave => $Valor) {
					$Lista[trim($Llave)] = (array_key_exists($Llave, $Matriz) == true) ? trim($Valor) : mb_strtoupper(trim($Valor));
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Estatico Publico
		 * DatosVacios($Array = false)
		 * 
		 * Recorre el array de datos POST o GET y regresa valor 
		 * @param $Array: array tomado como $_POST o $_GET
		 * @return true: encuentra valores vacios en el array
		 * @return false: No encuentra valores vacios
		 * @access public
		 * 
		 * */
		public static function DatosVacios($Array = false) {	
			if($Array == true) {
				foreach ($Array AS $Llave => $Valor) {
					$Lista[] = (empty($Valor) == true) ? '1' : '0'; 
				}
				return (array_sum($Lista)>=1) ? true : false;
			}
		}
		
		/**
		 * Metodo Estatico Publico
		 * DatosVaciosOmitidos($Array = false, $Omitidos = false)
		 * 
		 * Recorre el array de datos POST o GET y regresa valor 
		 * @param $Array: array tomado como $_POST o $_GET
		 * @param $Omitidos: array con las llaves que se omitiran
		 * @example array('omitir', 'omitir', 'omitir')
		 * @return true: encuentra valores vacios en el array
		 * @return false: No encuentra valores vacios
		 * @access public
		 * 
		 * */
		public static function DatosVaciosOmitidos($Array = false, $Omitidos = false) {	
			if($Array == true AND is_array($Array) == true AND $Omitidos == true AND is_array($Omitidos) == true) {
				$Matriz = (is_array($Omitidos) == true) ? array_flip($Omitidos) : array();
				foreach ($Array AS $Llave => $Valor) {
					$Lista[] = (array_key_exists($Llave, $Matriz) == true) ? '0' : (empty($Valor) == true) ? '1' : '0';
				}
				return (array_sum($Lista)>=1) ? true : false;
			}
		}
		
		/**
		 * Metodo Estatico Publico
		 * ConvertirTextoUcwords($Array = false);
		 * 
		 * Toma un Array y aplica funcion ucwords
		 * @param $Array: array de datos formatear
		 * 
		 * */
		public static function ConvertirTextoUcwords($Array = false) {
			if($Array == true AND is_array($Array) == true) {
				foreach ($Array AS $Llave => $Valor) {
					$Lista[trim($Llave)] = trim(mb_convert_case(mb_strtolower($Valor, "UTF-8"), MB_CASE_TITLE, "UTF-8"));
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Estatico Publico
		 * ConvertirTextoUcwordsOmitido($Array = false, $Omitidos = false);
		 * 
		 * Toma un Array y aplica funcion ucwords y omite los key ingresados
		 * @param $Array: array de datos formatear
		 * @param $Omitidos: array de campos a omitir
		 * 
		 * */
		public static function ConvertirTextoUcwordsOmitido($Array = false, $Omitidos = false) {	
			if($Array == true AND is_array($Array) == true AND $Omitidos == true AND is_array($Omitidos) == true) {
				$Matriz = (is_array($Omitidos) == true) ? array_flip($Omitidos) : array();
				foreach ($Array AS $Llave => $Valor) {
					$Lista[trim($Llave)] = (array_key_exists($Llave, $Matriz) == true) ? trim($Valor) : trim(mb_convert_case(mb_strtolower($Valor, "UTF-8"), MB_CASE_TITLE, "UTF-8"));
					//$Lista[trim($Llave)] = (array_key_exists($Llave, $Matriz) == true) ? trim($Valor) : trim(ucwords(strtolower($Valor)));
				}
				return $Lista;
			}
		}
			
		/**
		 * Metodo Publico
		 * LimpiarInyeccionSQL($Array = false)
		 * 
		 * Limpia de cadenas de inyeccion SQL a datos pasados por array
		 * @param $Array: array tomado como $_POST o $_GET
		 * 
		 * */
		public static function LimpiarInyeccionSQL($Array = false) {	
			if($Array == true AND is_array($Array) == true) {
				foreach ($Array AS $Llave => $Valor) {
					$Lista[trim($Llave)] = trim(self::SupresorSQL($Valor));
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Privado
		 * SupresorSQL($Datos)
		 * 
		 * Suprime las cadenas peligrosas de inyeccion SQL
		 * @param $Datos: Valor a Sanear
		 * 
		 * */
		private static function SupresorSQL($Datos) {	
			$Cadena = str_ireplace(' SELECT ', '', $Datos);
			$Cadena = str_ireplace(' COPY ', '', $Cadena);
			$Cadena = str_ireplace(' DELETE ', '', $Cadena);
			$Cadena = str_ireplace(' DROP ', '', $Cadena);
			$Cadena = str_ireplace(' DUMP ', '', $Cadena);
			$Cadena = str_ireplace('%', '', $Cadena);
			$Cadena = str_ireplace(' LIKE ', '', $Cadena);
			$Cadena = str_ireplace('--', '', $Cadena);
			$Cadena = str_ireplace('^', '', $Cadena);
			$Cadena = str_ireplace('[', '', $Cadena);
			$Cadena = str_ireplace(']', '', $Cadena);
			$Cadena = str_ireplace("\\", '', $Cadena);
			$Cadena = str_ireplace('!', '', $Cadena);
			$Cadena = str_ireplace('¡', '', $Cadena);
			$Cadena = str_ireplace('?', '', $Cadena);
			$Cadena = str_ireplace('=', '', $Cadena);
			$Cadena = str_ireplace('&', '', $Cadena);
			$Cadena = str_ireplace(' INSERT ', '', $Cadena);
			$Cadena = str_ireplace(' INTO ', '', $Cadena);
			$Cadena = str_ireplace(' VALUES ', '', $Cadena);
			$Cadena = str_ireplace(' FROM ', '', $Cadena);
			$Cadena = str_ireplace(' LEFT ', '', $Cadena);
			$Cadena = str_ireplace(' JOIN ', '', $Cadena);
			$Cadena = str_ireplace(' WHERE ', '', $Cadena);
			$Cadena = str_ireplace(' LIMIT ', '', $Cadena);
			$Cadena = str_ireplace(' ORDER BY ', '', $Cadena);
			$Cadena = str_ireplace(' DESC ', '', $Cadena);
			$Cadena = str_ireplace(' ASC ', '', $Cadena);
			$Cadena = addslashes($Cadena);
			return $Cadena;
		}
	}