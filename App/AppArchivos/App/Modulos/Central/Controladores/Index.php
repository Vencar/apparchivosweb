<?php
	
	/**
	 * Controlador: Index
	 */
	class Index extends Controlador {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
			//AppSession::ValSessionGlobal();
		}
		
		/**
		 * Metodo: Index
		 */
		public function Index() {
		    header('Location: ' .NeuralRutasApp::RutaUrlAppModulo('Administrador'));
		    //header('Location: ' .NeuralRutasApp::RutaUrlAppModulo('Usuario'));
		    exit();
		}
	}