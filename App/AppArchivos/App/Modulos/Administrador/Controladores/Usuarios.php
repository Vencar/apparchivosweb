<?php
/**
 * Created by PhpStorm.
 * User: IcemanIc300
 * Date: 08/06/2018
 * Time: 11:00 AM
 */

class Usuarios extends Controlador
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
    }

    /**
     * Metodo: Index
     * Se crea la instancia de la plantilla
     * Se muestra la vista pricipal del modulo
     * Administador
     */
    public function Index()
    {

    }

    public function formUsuario()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Perfiles = $this->Modelo->ConsultarPerfiles();
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Usuario', '* Campo requerido');
            $Validacion->Requerido('Nombre', '* Campo requerido');
            $Validacion->Requerido('ApellidoPaterno', '* Campo requerido');
            $Validacion->Requerido('Password', '* Campo requerido');
            $Validacion->Requerido('RepetirPassword', '* Campo requerido');
            $Validacion->Email('Correo', 'El Formato del Correo No Es Correcto');
            $Validacion->Numero('Telefono', 'Debe Ingresar un Valor Numerico');
            $Validacion->CampoIgual('Password', 'RepetirPassword', 'Las Contraseñas No Son Iguales ');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('formUsuario'));
            $Plantilla->Parametro('Token', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Perfiles', $Perfiles, APP);
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Usuarios', 'Registrar', 'RegistroUsuario.html')));
            unset($Plantilla);
        }
    }

    public function formEditar()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Perfiles = $this->Modelo->ConsultarPerfiles();
            $IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario']);
            $Informacion = $this->Modelo->ConsultarInformacionUsuario($IdUsuario)[0];
            $Informacion['IdUsuario'] = $IdUsuario;
            //$Informacion['Password'] = NeuralCriptografia::DeCodificar($Informacion['Password']);
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Usuario', '* Campo requerido');
            $Validacion->Requerido('Nombre', '* Campo requerido');
            $Validacion->Requerido('ApellidoPaterno', '* Campo requerido');
            $Validacion->Requerido('Password', '* Campo requerido');
            $Validacion->Requerido('RepetirPassword', '* Campo requerido');
            $Validacion->Email('Correo', 'El Formato del Correo No Es Correcto');
            $Validacion->Numero('Telefono', 'Debe Ingresar un Valor Numerico');
            $Validacion->CampoIgual('Password', 'RepetirPassword', 'Las Contraseñas No Son Iguales ');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('formEditar'));
            $Plantilla->Parametro('Token', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Perfiles', $Perfiles, APP);
            $Plantilla->Parametro('Informacion', $Informacion);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Usuarios', 'Editar', 'EditarUsuario.html')));
            unset($Plantilla);
        }
    }

    public function Registrar()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Token']) == true AND (NeuralCriptografia::DeCodificar($_POST['Token'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                unset($_POST['Token']);
                $Omitidos = ['ApellidoMaterno', 'Telefono', 'Correo'];
                if (!AppPost::DatosVaciosOmitidos($_POST, $Omitidos)) {
                    $DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
                    $this->GuardarUsuario($DatosPost);
                }
            }
        }
    }

    public function GuardarUsuario($Datos = false)
    {
        if (isset($Datos) and is_array($Datos)) {
            $DatosUsuario = ['Usuario' => $Datos['Usuario'], 'Password' => hash('sha256', $Datos['Password']), 'IdPerfil' => $Datos['IdPerfil']];
            unset($Datos['Usuario'], $Datos['Password'], $Datos['IdPerfil'], $Datos['RepetirPassword']);
            $IdUsuario = $this->Modelo->RegistrarUsuario($DatosUsuario);
            $Datos['IdUsuario'] = $IdUsuario;
            ($this->Modelo->GuardarInformacionUsuario($Datos) ? $this->Exito() : $this->Error());
        }
    }

    private function Exito()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Usuarios', 'Registrar', 'Mensajes', 'Correcto.html')));
    }

    private function ExitoEditar()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Usuarios', 'Editar', 'Mensajes', 'Correcto.html')));
    }

    private function Error()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Usuarios', 'Registrar', 'Mensajes', 'Error.html')));
    }

    private function ErrorEditar()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Usuarios', 'Editar', 'Mensajes', 'Error.html')));
    }

    public function EliminarUsuario()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario']);
            $Arreglo = ['Status' => 'ELIMINADO'];
            $Condicion = ['IdUsuario' => $IdUsuario];
            $this->Modelo->ActualizarEstadoUsuario($Arreglo, $Condicion);
            unset($IdUsuario, $Arreglo, $Condicion);
        }
    }

    public function EditarUsuario()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Token']) == true AND (NeuralCriptografia::DeCodificar($_POST['Token'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                unset($_POST['Token']);
                $_POST['IdUsuario'] = NeuralCriptografia::DeCodificar($_POST['IdUsuario']);
                $this->PrepararUsuario($_POST);
            }
        }
    }

    private function PrepararUsuario($Datos = false)
    {
        if ($Datos == true and is_array($Datos)) {
            $Omitidos = ['ApellidoMaterno', 'Telefono', 'Correo'];
            if (AppPost::DatosVaciosOmitidos($Datos, $Omitidos) == false) {
                $DatosPost = AppPost::LimpiarInyeccionSQL($Datos);
                $SistemaUsuarios = ['Usuario' => $DatosPost['Usuario'], 'Password' => $DatosPost['Password']];
                unset($DatosPost['Usuario'], $DatosPost['Password'], $DatosPost['RepetirPassword'],$DatosPost['IdPerfil']);
                $this->ActualizarUsuario($DatosPost, $SistemaUsuarios);
            }
        }
    }

    private function ActualizarUsuario($Informacion = false, $Credenciales = false)
    {
        if ($Informacion and is_array($Informacion) and $Credenciales and is_array($Credenciales)) {
            $Condicion = ['IdUsuario' => $Informacion['IdUsuario']];
            ($this->Modelo->ActualizarCredenciales($Credenciales, $Condicion) ?
                ($this->Modelo->ActualizarInforamcion($Informacion, $Condicion)) ?
                    $this->ExitoEditar() : $this->ErrorEditar() : $this->ErrorEditar());
        }
    }

}
