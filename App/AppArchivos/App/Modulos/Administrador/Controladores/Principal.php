<?php
/**
 * Created by PhpStorm.
 * User: IcemanIc300
 * Date: 01/06/2018
 * Time: 12:16 PM
 */

class Principal extends Controlador
{
    /**
     * Metodo: Index
     */
    public function Index()
    {

    }

    public function fnDashboard()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Principal', 'Principal.html')));
        unset($Plantilla);
    }

    public function ListadoUsuarios()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Usuarios = $this->Modelo->ConsultarUsuarios();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Usuarios', $Usuarios);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(['Usuarios', 'Listado', 'Listado.html']));
            unset($Plantilla);
        }
    }
}