<?php
	
	/**
	 * Controlador: Index
	 */
	class Index extends Controlador {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * Metodo: Index
		 */
		public function Index() {
			$Plantilla = new NeuralPlantillasTwig(APP);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Principal', 'Index.html')));
			unset($Plantilla);
		}
	}