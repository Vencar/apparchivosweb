<?php

/**
 * Controlador: Index
 */
class Documentos extends Controlador
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
    }

    /**
     * Metodo: Index
     * Se crea la instancia de la plantilla
     * Se muestra la vista pricipal del modulo
     * Administador
     */
    public function Index()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Principal', 'Index.html')));
        unset($Plantilla);
    }

    /**
     * Metodo: formAgregar
     * Se crea la instancia de la plantilla
     * Se muestra la vista del formulario para
     * Agregar un archivo
     */
    public function formAgregar()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Categoria = $this->Modelo->ConsultaCategorias();
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Titulo', '* Campo requerido');
            $Validacion->Requerido('IdCategoria', '* Campo requerido');
            $Validacion->Requerido('NombreArchivo', '* Necesita cargar archivo');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregar'));
            $Plantilla->Parametro('Token', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Categorias', $Categoria, APP);
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Documentos', 'Agregar', 'FormularioCargar.html')));
            unset($Plantilla);
        }
    }

    public function ActualizarDocumento()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Token']) == true AND (NeuralCriptografia::DeCodificar($_POST['Token'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                unset($_POST['Token']);
                $_POST['IdInformacionDocuento'] = NeuralCriptografia::DeCodificar($_POST['IdInformacionDocuento']);
                $this->ActualizarDatos($_POST);
            }
        }
    }

    private function ActualizarDatos($Datos = false)
    {
        if ($Datos == true and is_array($Datos)) {
            $Omitidos = ['Descripcion'];
            if (AppPost::DatosVaciosOmitidos($Datos, $Omitidos) == false) {
                $DatosPost = AppPost::LimpiarInyeccionSQL($Datos);
                $this->Categoria($DatosPost);
            }
        }
    }

    private function Categoria($Datos = false)
    {
        if (isset($Datos) and is_array($Datos)) {
            ((int)$Datos['IdCategoria'] > 0 ? $this->ActualizarInformacion($Datos) : $this->ActualizarCategoria($Datos));
        }
    }

    private function ActualizarInformacion($Datos = false)
    {
        if ($Datos == true and is_array($Datos)) {
            ($this->Modelo->ActualizarInformacionDocumento($Datos) ? $this->VistaActualizarExito() : $this->VistaActualizarError());
        }
    }

    private function ActualizarCategoria($Datos = false)
    {
        if ($Datos == true and is_array($Datos)) {
            $this->Modelo->AgregarCategoria($Datos['IdCategoria']);
            $IdCategoria = $this->Modelo->ConsultarIdCategoria($Datos['IdCategoria']);
            $Datos['IdCategoria'] = $IdCategoria[0]['IdCategoria'];
            $this->ActualizarInformacion($Datos);
            unset($Datos, $IdCategoria);
        }
    }

    public function formEditar()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Categoria = $this->Modelo->ConsultaCategorias();
            $InformacionDocumento =
                $this->Modelo->ConsultarInformacionDocumento(NeuralCriptografia::DeCodificar($_POST['IdDocumento']))[0];
            $NombreCategoria = $this->Modelo->ConsultarCategoria($InformacionDocumento['IdCategoria'])[0];
            $InformacionDocumento['NombreCategoria'] = $NombreCategoria['Descripcion'];
            unset($NombreCategoria);
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Titulo', '* Campo requerido');
            $Validacion->Requerido('IdCategoria', '* Campo requerido');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditar'));
            $Plantilla->Parametro('Token', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Categorias', $Categoria, APP);
            $Plantilla->Parametro('Informacion', $InformacionDocumento);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Documentos', 'Editar', 'FormularioEditar.html')));
            unset($Plantilla);
        }
    }

    /**
     * Metodo: Listado
     * Se llama al modelo para consultar los documentos existentes
     * se pasa el parametro a la plantilla
     * y se muestra la tabla con los documentos
     */
    public function Listado()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $ListadoDocumentos = $this->Modelo->ListarDocumentos();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Listado', $ListadoDocumentos, APP);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Documentos', 'Listado', 'Listado.html')));
            unset($Plantilla, $ListadoDocumentos);
        }
    }

    public function VizualizarDocumento()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $IdDocumento = $this->Modelo->ConsultarIdEditar(NeuralCriptografia::DeCodificar($_POST['IdDocumento']))[0];
            $RutaDocumento = $this->Modelo->ConsultarRutaDocumento($IdDocumento['IdDocumento'])[0];
            $this->ValidarExtencion($RutaDocumento);
        }
    }

    public function ValidarExtencion($Cadena = false)
    {
        if ($Cadena) {
            $Extencion = AppDropzone::ObtenerExtensionArchivo($Cadena['RutaArchivo']);
            $Ruta = AppDropzone::RutaArchivo($Cadena['RutaArchivo']);
            if ($Extencion == "pdf" || $Extencion == "PDF"){
                $this->VisualizarPDF($Ruta);
            }
            else{
                $this->VisualizarExcel($Ruta);
            }
        }
    }

    private function VisualizarPDF($Ruta){
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('Nombre', $Ruta, APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(['Documentos', 'Visualizar', 'Documento.html']));
        unset($IdDocumento, $RutaDocumento, $Ruta, $Plantilla);
    }

    private function VisualizarExcel($Ruta){
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('Nombre', $Ruta, APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(['Documentos', 'Visualizar', 'DocumentoExcel.html']));
        unset($IdDocumento, $RutaDocumento, $Ruta, $Plantilla);
    }


    /**
     * Metodo: Guardar
     * Se valida que sea una peticion ajax de la maquina
     * se verifica la llave y se manda al metodo VerificarAlmacenarDatos
     */
    public function Guardar()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Token']) == true AND (NeuralCriptografia::DeCodificar($_POST['Token'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                unset($_POST['Token']);
                $this->VerificarAlmacenarDatos($_POST);
            }
        }
    }

    /**
     * Metodo: VerificarAlmacenarDatos
     * Se limpian los datos de post para evitar
     * inyecciones sql y se evalua
     * si existe en la base de datos la categoria
     * si no existe manda a llamar al metodo para agregarla
     * si existe manda a llamar el metodo para guardar la informacion
     * del documento
     */
    private function VerificarAlmacenarDatos($Datos = false)
    {
        if ($Datos == true and is_array($Datos)) {
            $Omitidos = ['Descripcion'];
            if (AppPost::DatosVaciosOmitidos($Datos, $Omitidos) == false) {
                $DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), $Omitidos);
                ($this->DuplicidadTitulo($DatosPost['Titulo'])['Cantidad'] == 0 ? $this->VerificarCategoria($DatosPost) : $this->VistaError());
                unset($Datos, $DatosPost, $IdDocumento);
            }
        }
    }

    private function VerificarCategoria($Datos = false)
    {
        if (isset($Datos) and is_array($Datos)) {
            ((int)$Datos['IdCategoria'] > 0 ? $this->InsertarInformacion($Datos) : $this->AgregarCategoria($Datos));
        }
    }

    private function DuplicidadTitulo($Titulo = false)
    {
        if ($Titulo == true) {
            return $this->Modelo->ConsultarTituloExistente($Titulo);
        }
    }

    /**
     * Metodo: InsertarInformacion
     * Se obtiene el id del documento almacenado
     * Se prepara el arreglo para insertar la
     * informacion del documento
     * maneja las vistas de exito y error
     */
    public function InsertarInformacion($Datos = false)
    {
        if ($Datos == true AND is_array($Datos)) {
            $IdDocumento = $this->Modelo->ConsultarIdDocumento($Datos['NombreArchivo']);
            unset($Datos['NombreArchivo']);
            $Datos['IdDocumento'] = $IdDocumento[0]['IdDocumento'];
            ($this->Modelo->AlmacenarInformacion($Datos) ? $this->VistaExito() : $this->VistaError());
        }
    }

    /**
     * Metodo: AgregarCategoria
     * Inserta una cateogira y obtiene su id
     * Hace el llamado para insertar la informacion del documento
     *
     */
    public function AgregarCategoria($Datos = false)
    {
        if ($Datos == true AND is_array($Datos)) {
            $this->Modelo->AgregarCategoria($Datos['IdCategoria']);
            $IdCategoria = $this->Modelo->ConsultarIdCategoria($Datos['IdCategoria']);
            $Datos['IdCategoria'] = $IdCategoria[0]['IdCategoria'];
            $this->InsertarInformacion($Datos);
            unset($Datos, $IdCategoria);
        }
    }

    /**
     * Metodo: VistaExito
     * Muestra la plantilla
     * de documento agregado
     * correctamente
     */
    private function VistaExito()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Documentos', 'Agregar', 'Mensajes', 'Correcto.html')));
    }

    private function VistaActualizarExito()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Documentos', 'Editar', 'Mensajes', 'Correcto.html')));
    }

    /**
     * Metodo: VistaError
     * Muestra la plantilla
     * de error al cargar el documento
     */
    private function VistaError()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Documentos', 'Agregar', 'Mensajes', 'Error.html')));

    }

    private function VistaActualizarError()
    {
        $Plantilla = new NeuralPlantillasTwig(APP);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Documentos', 'Editar', 'Mensajes', 'Error.html')));
    }

    /**
     * Metodo: GuardarArchivo
     * Se encarga de guardar el archivo
     * subido mediante Dropzone
     * y recuperar el nombre para pasarlo al formulario
     * para agregar la informacion del documento
     */
    public function GuardarArchivo()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (is_array($_FILES) == true) {
                if ($NombreArchivo = AppDropzone::SubirArchivo($_FILES)) {
                    $this->Modelo->GuardarDocumento($NombreArchivo);
                    $Nombre = json_encode(['Nombre' => $NombreArchivo]);
                    header('Content-Type: application/json');
                    echo $Nombre;
                    unset($NombreArchivo, $Nombre);
                } else {
                    $Nombre = json_encode(['Nombre' => "Invalido"]);
                    header('Content-Type: application/json');
                    echo $Nombre;
                    unset($NombreArchivo, $Nombre);
                }
            }
        }

    }

    public function EliminarDocumento()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdDocumento']) == true) {
                $IdDocumento = NeuralCriptografia::DeCodificar($_POST['IdDocumento']);
                $Arreglo = array('Status' => 'ELIMINADO');
                $Condicion = array('IdInformacionDocumento' => $IdDocumento);
                $this->Modelo->ActualizarEstadoDocumento($Arreglo, $Condicion);
                unset($IdDocumento, $Arreglo, $Condicion);
            }
        }
    }

}