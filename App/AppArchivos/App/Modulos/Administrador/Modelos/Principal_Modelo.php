<?php
/**
 * Created by PhpStorm.
 * User: IcemanIc300
 * Date: 11/06/2018
 * Time: 10:55 AM
 */

class Principal_Modelo extends Modelo
{

    function __Construct()
    {
        parent::__Construct();
    }

    public function ConsultarUsuarios() {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_informacion_usuarios');
        $Consulta->Columnas('IdUsuario, Nombres, ApellidoPaterno as Paterno, ApellidoMaterno as Materno,
                            Telefono, Correo');
        $Consulta->Condicion("Status = 'ACTIVO'");
        return $Consulta->Ejecutar(false, true);
    }

}