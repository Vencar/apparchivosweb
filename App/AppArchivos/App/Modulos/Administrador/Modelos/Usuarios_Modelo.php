<?php

class Usuarios_Modelo extends Modelo
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
    }

    public function ConsultarPerfiles()
    {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_sistema_usuarios_perfil');
        $Consulta->Columnas('IdPerfil, Nombre');
        return $Consulta->Ejecutar(false, true);
    }

    public function RegistrarUsuario($Datos = false)
    {
        if (isset($Datos) and is_array($Datos)) {
            $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
            try {
                $Conexion->insert('tbl_sistema_usuarios', $Datos);
                return $Conexion->lastInsertId();
            } catch (PDOException $ex) {
            }
        }
    }

    public function GuardarInformacionUsuario($Datos = false)
    {
        if (isset($Datos) and is_array($Datos)) {
            $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
            try {
                $Conexion->insert('tbl_informacion_usuarios', $Datos);
                return true;
            } catch (PDOException $ex) {
            }
        }
    }

    public function ActualizarEstadoUsuario($Status = false, $Condicion = false)
    {
        if (isset($Status) and is_array($Status) and isset($Condicion) and is_array($Condicion)) {
            $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
            try {
                $Conexion->update('tbl_informacion_usuarios', $Status, $Condicion);
            } catch (Exception $e) {
                Ayudas::print_r($e);
            }
        }
    }

    public function ActualizarCredenciales($Credenciales = false, $Condicion = false) {
        if($Credenciales and is_array($Credenciales) and $Condicion and is_array($Condicion)){
            $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
            try {
                $Conexion->update('tbl_sistema_usuarios', $Credenciales, $Condicion);
                return true;
            } catch (Exception $e){
                Ayudas::print_r($e->getMessage());
                return false;
            }
        }
    }

    public function ActualizarInforamcion($Informacion = false, $Condicion = false){
        if($Informacion and is_array($Informacion) and $Condicion and is_array($Condicion)){
            $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
            try{
                $Conexion->update('tbl_informacion_usuarios', $Informacion, $Condicion);
                return true;
            }catch (Exception $e){
                Ayudas::print_r($e->getMessage());
                return false;
            }
        }
    }

    public function ConsultarInformacionUsuario($IdUsuario = false)
    {
        if ($IdUsuario) {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_sistema_usuarios');
            $Consulta->Columnas('IdPerfil as Perfil, Usuario, Password, Nombres, ApellidoPaterno as Paterno, 
                                ApellidoMaterno as Materno, Telefono, Correo');
            $Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario',
                                    'tbl_informacion_usuarios.IdUsuario');
            $Consulta->Condicion("tbl_sistema_usuarios.IdUsuario = '$IdUsuario' ");
            return $Consulta->Ejecutar(false, true);
        }
    }
}