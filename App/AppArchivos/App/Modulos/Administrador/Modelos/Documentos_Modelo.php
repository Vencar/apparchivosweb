<?php

/**
 * Clase: Index_Modelo
 */
class Documentos_Modelo extends Modelo
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
    }

    /**
     * Metodo: Ejemplo
     */
    public function ConsultaSQL()
    {

    }

    public function ConsultaCategorias()
    {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_categorias');
        $Consulta->Columnas('IdCategoria, Descripcion');
        return $Consulta->Ejecutar(false, true);
    }

    public function ConsultarCategoria($IdCategoria = false){
        if ($IdCategoria == true){
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_categorias');
            $Consulta->Columnas('Descripcion');
            $Consulta->Condicion("IdCategoria = '$IdCategoria'");
            return $Consulta->Ejecutar(false, true);
        }
    }

    public function ListarDocumentos()
    {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_informacion_documentos');
        $Consulta->Columnas('IdInformacionDocumento, Titulo, Descripcion, Tipo, NombreCompleto');
        $Consulta->InnerJoin('tbl_documentos',
            'tbl_informacion_documentos.IdDocumento', 'tbl_documentos.IdDocumento');
        $Consulta->Condicion("tbl_informacion_documentos.Status = 'ACTIVO'");
        return $Consulta->Ejecutar(false, true);
    }

    public function ConsultarIdCategoria($Nombre = false)
    {
        if ($Nombre == true) {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_categorias');
            $Consulta->Columnas('IdCategoria');
            $Consulta->Condicion("Descripcion = '$Nombre' ");
            return $Consulta->Ejecutar(true, true);
        }
    }

    public function ConsultarIdDocumento($NombreDocumento = false)
    {
        if ($NombreDocumento == true) {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_documentos');
            $Consulta->Columnas('IdDocumento');
            $Consulta->Condicion("RutaArchivo = '$NombreDocumento' ");
            return $Consulta->Ejecutar(true, true);
        }
    }

    public function ConsultarInformacionDocumento($IdInformacion = false)
    {
        if ($IdInformacion == true) {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_informacion_documentos');
            $Consulta->Columnas('IdInformacionDocumento, Titulo, Descripcion, IdCategoria');
            $Consulta->Condicion("IdInformacionDocumento = '$IdInformacion'");
            $Consulta->Condicion("Status = 'ACTIVO'");
            return $Consulta->Ejecutar(false, true);
        }
    }

    public function ActualizarInformacionDocumento($Datos = false){
        if (isset($Datos) AND is_array($Datos)){
            $SQL = new NeuralBDGab(APP, 'tbl_informacion_documentos');
            $SQL->Sentencia('Titulo', $Datos['Titulo']);
            $SQL->Sentencia('Descripcion', $Datos['Descripcion']);
            $SQL->Sentencia('IdCategoria', $Datos['IdCategoria']);
            $SQL->Condicion('IdInformacionDocumento', $Datos['IdInformacionDocuento']);
            return $SQL->Actualizar();
        }
    }

    public function ConsultarIdEditar($IdInfo = false)
    {
        if ($IdInfo == true) {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_informacion_documentos');
            $Consulta->Columnas('IdDocumento');
            $Consulta->Condicion("IdInformacionDocumento = '$IdInfo' ");
            return $Consulta->Ejecutar(false, true);
        }
    }

    public function ConsultarTituloExistente($Titulo)
    {
        if ($Titulo == true) {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_informacion_documentos');
            $Consulta->Columnas('Titulo');
            $Consulta->Condicion("Titulo = '$Titulo' ");
            return $Consulta->Ejecutar(true, false);
        }
    }

    public function ConsultarRutaDocumento($IdDocumento = false)
    {
        if ($IdDocumento == true) {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_documentos');
            $Consulta->Columnas('RutaArchivo');
            $Consulta->Condicion("IdDocumento = '$IdDocumento' ");
            return $Consulta->Ejecutar(false, true);
        }
    }

    public function ActualizarEstadoDocumento($Status = false, $Condicion = false)
    {
        $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
        try {
            $Conexion->update('tbl_informacion_documentos', $Status, $Condicion);
        } catch (Exception $e) {
            Ayudas::print_r($e);
        }
    }

    public function GuardarInformacionDocumento($Arreglo = false)
    {
        if ($Arreglo == true AND is_array($Arreglo)) {
            ((int)$Arreglo['IdCategoria'] > 0)
                ? $this->AlmacenarInformacion($Arreglo) : $this->AgregarCategoria($Arreglo['IdCategoria']);
        }
    }

    public function AlmacenarInformacion($Datos = false)
    {
        if ($Datos == true AND is_array($Datos)) {
            $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
            try {
                $Conexion->insert('tbl_informacion_documentos', $Datos);
                return true;
            } catch (Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function AgregarCategoria($Arreglo = false)
    {
        if ($Arreglo == true) {
            $Datos = ['Descripcion' => $Arreglo];
            $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
            try {
                $Conexion->insert('tbl_categorias', $Datos);
                return $Conexion->lastInsertId();
            } catch (PDOException $ex) {
            }

        }
    }

    public function GuardarDocumento($NombreDocumento = false)
    {
        if ($NombreDocumento == true) {
            $Tipo = AppDropzone::ObtenerExtensionArchivo($NombreDocumento['RutaArchivo']);
            $Datos = ['Tipo' => $Tipo, 'RutaArchivo' => $NombreDocumento['RutaArchivo'], 'NombreCompleto' => $NombreDocumento['Original']];
            $Conexion = NeuralConexionDB::DoctrineDBAL(APP);
            try {
                $Conexion->insert('tbl_documentos', $Datos);
                return $Conexion->lastInsertId();
            } catch (PDOException $ex) {
            }
        }
    }

}