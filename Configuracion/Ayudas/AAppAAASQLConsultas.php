<?php

	/**
	 * VoIP Technology S.A.
	 * CALL ONE
	 *
	 * @copyright Copyright (c) 2015, VoIP Technology S.A.
	 * @link   http://voiptech.com.mx
	 * @author Ramses Aguirre Farrera <ramsesaguirre2012@gmail.com>
	 * @author Contacto <contacto#voiptech.com.mx>
	 * @license Contrato de Licencia de Software de Usuario Final (“EULA”).
	 * @license Incluida licencia carpeta de Informacion
	 * @version 1.0
	 *
	 * Este contrato de licencia de software de usuario final (EULA, por sus siglas en inglés)
	 * es un acuerdo vinculante entre el usuario titular de la licencia (“Usuario final”) y VoIP Technology S.A.,
	 * que expone los términos y condiciones que rigen el uso y el funcionamiento de los productos
	 * de software de computadoras propiedad de CallOne Contact Center (el “Software”) y las especificaciones técnicas
	 * escritas para el uso y el funcionamiento del Software (la “Documentación”). Donde el sentido
	 * y el contexto lo permitan, las referencias en este EULA al Software incluyen la Documentación.
	 * Al descargar e instalar, copiar o, en otras palabras, usar el Software y/o aceptar este EULA,
	 * el Usuario final acuerda reconocer como vinculante los términos y condiciones de este EULA.
	 *
	 * Si el Usuario final no acuerda ni acepta los términos de este EULA, es posible que el Usuario
	 * final no tenga acceso ni pueda usar el Software.
	 *
	 */
	
	class AppSQLConsultas extends Modelo {
		
		/**
		 * Metodo Contructor
		 * 
		 */
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * Metodo Privado
		 * ListarColumnas($Tabla = false, $Omitidos = false)
		 * 
		 * @param $Tabla: Nombre de la Tabla a Listar las Columnas
		 * @param $Omitidos: array incremental con el nombre de las columnas a omitir
		 * @param $Alias: es un array asociativo
		 * @example array('Columna' => 'Alias')
		 * @param $Apliacion: Arreglo incrementa de la aplicacion si no se especifica se toma el del modulo
		 * @example array('APLICACION')
         * 
		 */
		public function ListarColumnas($Tabla = false, $Omitidos = false, $Alias = false, $Aplicacion = 'DEFAULT') {
			if($Tabla == true) {
				$Conexion = NeuralConexionDB::DoctrineDBAL($Aplicacion);
				$Consulta = $Conexion->prepare("DESCRIBE $Tabla");
				$Consulta->execute();
				$Lista = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Cantidad = $Consulta->rowCount();
				$Matriz = (is_array($Omitidos) == true) ? array_flip($Omitidos) : array();
				$AliasBase = (is_array($Alias) == true) ? $Alias : array();
				for ($i=0; $i<$Cantidad; $i++) {
					if(array_key_exists($Lista[$i]['Field'], $Matriz) == false) {
						if(array_key_exists($Lista[$i]['Field'], $AliasBase) == true) {
							$Columna[] = $Tabla.'.'.$Lista[$i]['Field'].' AS '.$AliasBase[$Lista[$i]['Field']];
						}
						else {
							$Columna[] = $Tabla.'.'.$Lista[$i]['Field'];
						}
					}
				}
				//return implode(', ', $Columna);
				return $Columna;
			}
		}

		/**
		 * Metodo Privado
		 * ListarNombreColumnas($Tabla = false, $Omitidos = false, $Alias = false)
		 *
		 * Devuelve un array con los nombres de los campos de la tabla en cada posicion del arreglo.
		 * @param bool|false $Tabla: Nombre de la tabla
		 * @param bool|false $Omitidos: Elementos Omitidos, array('NombreCampoOmitido', ...).
		 * @param bool|false $Alias: Si es requerido un alias, array('Columna' => 'Alias', ...).
		 * @return array con el nombre de los campos de la tabla
		 */
		private function ListarNombreColumnas($Tabla = false, $Omitidos = false, $Alias = false, $Aplicacion = 'DEFAULT') {
			if($Tabla == true) {
				$Conexion = NeuralConexionDB::DoctrineDBAL($Aplicacion);
				$Consulta = $Conexion->prepare("DESCRIBE $Tabla");
				$Consulta->execute();
				$Lista = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Cantidad = count($Lista);
				$Matriz = (is_array($Omitidos) == true) ? array_flip($Omitidos) : array();
				$AliasBase = (is_array($Alias) == true) ? $Alias : array();
				for ($i=0; $i<$Cantidad; $i++) {
					if(array_key_exists($Lista[$i]['Field'], $Matriz) == false) {
						if(array_key_exists($Lista[$i]['Field'], $AliasBase) == true) {
							$Columna[] = $Lista[$i]['Field'].' AS '.$AliasBase[$Lista[$i]['Field']];
						}
						else {
							$Columna[] = $Lista[$i]['Field'];
						}
					}
				}
				return $Columna;
			}
		}

		/**
		 * Metodo publico
		 * ObtenerArregloOmitidos($Datos = false)
		 *
		 * Retorna el arreglo con los campos de la tabla, menos los elementos llave(campo o campos) que contiene el arreglo datos.
		 * @param bool|false $Datos: Arreglo de datos, array('Campo' => 'Valor', ...);
		 * @return array: array('CampoOmitido1', 'CampoOmitido2', ...);
		 */
		public function ObtenerArregloOmitidos($Datos = false, $Tabla = false, $Aplicacion = false){
			if($Datos == true AND is_array($Datos) AND $Tabla == true AND $Aplicacion == true){
				return self::ListarNombreColumnas($Tabla, array_keys($Datos), false, $Aplicacion);
			}
			else{
				return array();
			}

		}
		
		/**
		 * Metodo Publico
		 * GuardarDatos($Array = false, $Tabla = false, $Omitidos = false, $Aplicacion = 'DEFUALT');
		 * 
		 * Guarda Datos en la tabla seleccionada
		 * @param $Array: array asociativo con los datos a guardar
		 * @param $Tabla: donde se guardaran los datos
		 * @param $Omitidos: array incremental con las columnas omitidas donde no se validara
		 * @example array('id', 'nombre', 'apellidos')
		 * @param $Aplicacion: Aplicacion que se utilizara para conexion a BD
		 * 
		 * */
		public function GuardarDatos($Array = false, $Tabla = false, $Omitidos = false, $Aplicacion = 'DEFAULT') {	
			if($Array == true AND is_array($Array) == true AND $Tabla == true AND is_array($Omitidos) == true AND $Aplicacion == true) {
				$Matriz = self::ListarColumnasTabla($Tabla, $Omitidos, 'ARRAY', $Aplicacion);
				if(count($Array) == count($Matriz)) {
					$SQL = new NeuralBDGab($Aplicacion, $Tabla);
					while(list($Columna, $Valor) = each($Array)) {
						if(array_key_exists($Columna, $Matriz) == true) {
							$SQL->Sentencia($Columna, $Valor);
						}
					}
					$SQL->Insertar();
					unset($Aplicacion, $Array, $Omitidos, $Tabla, $Columna, $Matriz, $SQL, $Valor);
					return false;
				}
				else {
					return 'Las Columnas No Coinciden Con la Cantidad de Datos Recibidos';
				}	
			}
		}
		
		/**
		 * Metodo Publico
		 * ActualizarDatos($Array = false, $Condiciones = false, $Tabla = false, $Omitidos = false, $Aplicacion = 'DEFULAT');
		 * 
		 * Guarda Datos en la tabla seleccionada
		 * @param $Array: array asociativo con los datos a actualizar
		 * @param $Condiciones: array asociativo con las condiciones a cumplir
		 * @example array('id' => '2', 'estado' => 'ACTIVO')
		 * @param $Tabla: donde se guardaran los datos
		 * @param $Omitidos: array incremental con las columnas omitidas donde no se validara
		 * @example array('id', 'nombre', 'apellidos')
		 * @param $Aplicacion: Aplicacion que se utilizara para conexion a BD
		 * 
		 * */
		public function ActualizarDatos($Array = false, $Condiciones = false, $Tabla = false, $Omitidos = false, $Aplicacion = 'DEFULAT') {	
			if($Array == true AND is_array($Array) == true AND is_array($Condiciones) == true AND $Tabla == true AND is_array($Omitidos) == true AND $Aplicacion == true) {
				$Matriz = self::ListarColumnasTabla($Tabla, $Omitidos, 'ARRAY', $Aplicacion);
				if(count($Array) == count($Matriz)) {
					$SQL = new NeuralBDGab($Aplicacion, $Tabla);
					foreach ($Array AS $Columna => $Valor) {
						if(array_key_exists($Columna, $Matriz) == true) {
							$SQL->Sentencia($Columna, $Valor);
						}
					}
					foreach ($Condiciones AS $CColumna => $CValor) {
						$SQL->Condicion($CColumna, $CValor);
					}
					$SQL->Actualizar();
					unset($Aplicacion, $Array, $Condiciones, $Omitidos, $Tabla, $CColumna, $CValor, $Matriz, $SQL, $Valor);
				}
				else {
					return 'Las Columnas No Coinciden Con la Cantidad de Datos Recibidos';
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * EliminarDatos($Condiciones = false, $Tabla = false, $Aplicacion = 'DEFUALT');
		 * 
		 * Elimina los datos seleccionados
		 * @param $Condiciones: array asociativo con las condiciones correspondientes
		 * @example array('id' => '4', 'Estado' => 'ACTIVO')
		 * @param $Tabla: tabla donde se realizara el procedimiento
		 * @param $Aplicacion: aplicacion donde se tomaran los datos de BD
		 * 
		 * */
		public function EliminarDatos($Condiciones = false, $Tabla = false, $Aplicacion = 'DEFUALT') {	
			if(is_array($Condiciones) == true AND $Tabla == true AND $Aplicacion == true) {
				$SQL = new NeuralBDGab($Aplicacion, $Tabla);
				while(list($Columna, $Valor) = each($Condiciones)) $SQL->Condicion($Columna, $Valor);
				$SQL->Eliminar();
			}
		}
		
		/**
		 * Metodo Privado
		 * ListarComentariosColumTabla($Tabla = false)
		 * 
		 * Lista los comentarios de la tabla
		 * @param $Tabla: Nombre de la tabla
		 * 
		 * */
		private function ListarComentariosColumTabla($Tabla = false) {	
			if($Tabla == true) {
				$Conexion = NeuralConexionDB::DoctrineDBAL($Aplicacion);
				$Consulta = $Conexion->prepare("SELECT COLUMN_COMMENT FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$Tabla'");
				$Datos = $Consulta->execute();
				$Cantidad = count($Datos);
				for ($i=0; $i<$Cantidad; $i++) {
					$Lista['0'][] = $Datos[$i]['COLUMN_COMMENT'];
				}
				unset($Tabla, $Cantidad, $Consulta, $Datos);
				return $Lista;
			}
		}
		
		/**
		 * Metodo Privado
		 * ListarColumnasTabla($Tabla = false, $Omitidos = array() ,$Tipo = 'LISTA')
		 * 
		 * Lista las columnas de una tabla segun la necesidad
		 * @param $Tabla: Nombre de la Tabla a Listar las Columnas
		 * @param $Omitidos: array incremental con el nombre de las columnas a omitir
		 * @param $Tipo: el tipo de datos que requerimos
		 * @return LISTA: Lista ordenada separa por comas
		 * @return ARRAY: devuelve un array asociativo con el nombre de las columnas
		 * 
		 * */
		private function ListarColumnasTabla($Tabla = false, $Omitidos = array() ,$Tipo = 'LISTA', $Aplicacion = 'DEFAULT') {	
			if($Tabla == true AND is_array($Omitidos) == true AND $Tipo == true AND $Aplicacion == true) {
				$Conexion = NeuralConexionDB::DoctrineDBAL($Aplicacion);
				$Consulta = $Conexion->prepare("DESCRIBE $Tabla");
				$Consulta->execute();
				$Datos = $Consulta->fetchAll();
				$Cantidad = count($Datos);
				$Matriz = array_flip($Omitidos);
				for ($i=0; $i<$Cantidad; $i++) {
					if(array_key_exists($Datos[$i]['Field'], $Matriz) == false) {
						$Lista[] = $Datos[$i]['Field'];
					}
				}
				unset($Tabla, $Omitidos, $Consulta, $Datos, $Cantidad, $Matriz, $Aplicacion);
				if($Tipo == 'LISTA') {
					return implode(', ', $Lista);
				}
				else {
					return array_flip($Lista);
				}
			}
		}

		/**
		 * Metodo publico
		 * ObtenerCondicionesOR($ArregloAsociativo = false, $Campo = false)
		 *
		 * Obtiene una cadena condicional para una consulta OR.
		 * @param bool|false $ArregloAsociativo: arreglo que puede tener las columnas asociadas o solo valores.
		 * @param bool|false $Campo: Si el arreglo con indices asociados no tiene el nombre de las columnas,
		 * $Campo debe especificar el campo de la tabla.
		 * @return string:
		 */
		public static function ObtenerCondicionesOR($ArregloAsociativo = false, $Campo = false, $OperadorCondicional = '='){
			if(isset($ArregloAsociativo) == true AND is_array($ArregloAsociativo) == true){
				$Condicion = ''; $Contador = 0;
				foreach($ArregloAsociativo as $Llave => $Valor){
					if($Contador == 0){
						if($Campo == true)
							$Condicion = $Campo . " $OperadorCondicional '".$Valor."'";
						else
							$Condicion = $Llave . " $OperadorCondicional '".$Valor."'";
					}else{
						if($Campo == true)
							$Condicion = $Condicion." OR ".$Campo . " $OperadorCondicional '".$Valor."'";
						else
							$Condicion = $Condicion." OR ".$Llave . " $OperadorCondicional '".$Valor."'";
					}
					$Contador = $Contador + 1;
				}
				return $Condicion;
			}

		}

		/**
		 * Metodo publico
		 * ObtenerCondicionesAND($ArregloAsociativo = false, $Campo = false)
		 *
		 * Obtiene una cadena condicional para una consulta AND.
		 * @param bool|false $ArregloAsociativo: arreglo que puede tener las columnas asociadas o solo valores.
		 * @param bool|false $Campo: Si el arreglo con indices asociados no tiene el nombre de las columnas
		 * el nombre de la columna se debe pasar en el segundo parametro.
		 * @return string
		 */
		public static function ObtenerCondicionesAND($ArregloAsociativo = false, $Campo = false, $OperadorCondicional = '='){
			if(isset($ArregloAsociativo) == true AND is_array($ArregloAsociativo) == true){
				$Condicion = ''; $Contador = 0;
				foreach($ArregloAsociativo as $Llave => $Valor){
					if($Contador == 0){
						if($Campo == true)
							$Condicion = '('.$Campo.' '.$OperadorCondicional.' "'.$Valor.'")';
						else
							$Condicion = '('.$Llave .' '.$OperadorCondicional.' "'.$Valor.'")';
					}else{
						if($Campo == true)
							$Condicion = $Condicion.' AND ('.$Campo .' '.$OperadorCondicional.' "'.$Valor.'")';
						else
							$Condicion = $Condicion.' AND ('.$Llave .' '.$OperadorCondicional.' "'.$Valor.'")';
					}
					$Contador = $Contador + 1;
				}
				return $Condicion;
			}
		}

		/**
		 * Metodo Publico
		 * ObtenerArrayCondiciones($ArregloDatosPost = false, $Condiciones = false)
		 *
		 * Obtiene el arreglo de condiciones para la consulta.
		 * @param bool|false $ArregloDatosPost; Datos del formulario, con el valor de las condiciones
		 * @param bool|false $Condiciones: arreglo con los campos de busqueda. array('campoBD'=>'TipoValidacion', ...)
		 * @return array: array condicionales. array('campoBD'=>'ValorCondicion');
		 */
		public static function ObtenerArrayCondiciones($ArregloDatosPost = false, $Condiciones = false, $Tabla = false){
			if(isset($ArregloDatosPost) == true and is_array($ArregloDatosPost) == true AND isset($Condiciones) == true AND is_array($Condiciones)){
				$Criterios = array();
				foreach($Condiciones as $Llave => $Valor){
					if(array_key_exists($Llave, $ArregloDatosPost) == true AND ($ArregloDatosPost[$Llave] != '') == true ){
						switch($Valor){
							case 'Texto':
								$Criterios[] = $Tabla.'.'.$Llave.' LIKE "'.AppFormatos::FormatoTexto($ArregloDatosPost[$Llave]).'"';
								break;
							case 'Entero':
								$Criterios[] = $Tabla.'.'.$Llave.' LIKE "'.AppFormatos::FormatoEntero($ArregloDatosPost[$Llave]).'"';
								break;
							case 'Telefono':
								$Criterios[] = $Tabla.'.'.$Llave.' LIKE "'.AppFormatos::FormatoTelefono($ArregloDatosPost[$Llave]) . '"';
								break;
							case 'Like':
								$Criterios[] = $Tabla.'.'.$ArregloDatosPost[$Llave].' LIKE "%'.trim($ArregloDatosPost['Valor'.$Llave]).'%"';
								break;
							case 'Identificador':
								$Criterios[] = $Tabla.".".$Llave." = '".NeuralCriptografia::DeCodificar($ArregloDatosPost[$Llave])."'";
								break;
							case 'IdentificadorCampania':
								$Criterios[] = 'tbl_campanias'.".".$Llave." = '".NeuralCriptografia::DeCodificar($ArregloDatosPost[$Llave])."'";
								break;
							case 'RangoFecha':
								$Rango = AppFiltrosRapidos::ObtenerRango($ArregloDatosPost[$Llave]);
								foreach($Rango as $Columna => $value){
									$Rango[$Columna] = AppFechas::FormatoFecha($value);
								}
								$Criterios[] = AppFiltrosRapidos::CondicionesRango($Rango[0], $Rango[1], $Tabla.".".$Llave);
								break;						
						}
					}
				}
				return $Criterios;
			}
		}

		/**
		 * Metodo Publico
		 * ObtenerCondicionesLike($ArrayCampos = false, $Criterio,  $Campo = false)
		 *
		 * Obtiene condiciones like para un criterio de búsqueda.
		 * @param bool|false $ArrayCampos: campos que se van a buscar en la Base de Datos.
		 * @param $Criterio: Valor que se busca en ArrayCampos();
		 * @param bool|false $Campo: (Opcional) Si se va a realizar la búsqueda sobre un solo campo.
		 * @return string
		 */
		public static function ObtenerCondicionesLike($ArrayCampos = false, $Criterio = false,  $Campo = false){
			if($ArrayCampos == true AND is_array($ArrayCampos) == true AND $Criterio != ""){
				$CamposLikes = '';
				$Contador = 0;
				foreach ($ArrayCampos as $Llave => $Valor) {
					if ($Contador == 0) {
						if($Campo == true)
							$CamposLikes = '('.$Campo . ' LIKE "%'.$Criterio.'%")';
						else
							$CamposLikes = '(' . $Valor . ' LIKE "%'.$Criterio.'%")';
					} else {
						if($Campo == true)
							$CamposLikes = $CamposLikes . ' OR (' . $Campo . ' LIKE "%'.$Criterio.'%")';
						else
							$CamposLikes = $CamposLikes . ' OR (' . $Valor . ' LIKE "%'.$Criterio.'%")';
					}
					$Contador = $Contador + 1;
				}
				return "$CamposLikes";
			}
		}

		/**
		 * Metodo Publico
		 * ObtenerCondicionesLikeByArray($DatosPost = false)
		 *
		 * Obtiene condicionales Like para la sentencia WHERE
		 * @param bool|false $DatosPost: arreglo de datos y campos para busquedas.
		 * @return string: (Nombres LIKE "%Yadira%") OR (ApellidoPaterno LIKE "%Roman%") OR (ApellidoMaterno LIKE "%Lopez%")
		 */
		public static function ObtenerCondicionesLikeByArray($DatosPost = false){
			if($DatosPost == true AND is_array($DatosPost)){
				$CamposLikes = '';
				$Contador = 0;
				foreach ($DatosPost as $Llave => $Valor) {
					if($Valor != ''){
						if ($Contador == 0) {
							$CamposLikes = '(' . $Llave . ' LIKE "%'.$Valor.'%")';
						} else {
							$CamposLikes = $CamposLikes . ' OR (' . $Llave . ' LIKE "%'.$Valor.'%")';
						}
						$Contador = $Contador + 1;
					}
				}
				return $CamposLikes;
			}
		}

	}